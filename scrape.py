import asyncio
from aiohttp import ClientSession
import sys
from functools import partial, reduce, singledispatch
import itertools
import time
from operator import add
import attr
from attr.validators import instance_of
from validators import url
import json
import pprint
import tqdm
import jsonlines

def to_serializable(val):
    """Used by default."""
    if attr.has(val):
        # attr.asdict is available!
        return attr.asdict(val)
    return str(val)

@attr.s
class CKANEntitySearchable(object):
    api_url = attr.ib(validator=lambda *args: url(args[-1]))
    metadata = attr.ib()
    timestamp = attr.ib(default=attr.Factory(time.time), validator=instance_of(float))
    name = attr.ib(init=False)
    @name.default
    def _extract_name(self):
        return self.metadata['name']
    
@attr.s
class CKANEntityNonSearchable(object):
    api_url = attr.ib(validator=lambda *args: url(args[-1]))
    max_retries = attr.ib(validator=instance_of(int))
    # name property for all but resource (which only allows _show by id)
    identifier = attr.ib(validator=instance_of(str))
    metadata = attr.ib(default=attr.Factory(dict))
    timestamp = attr.ib(default=attr.Factory(time.time), validator=instance_of(float))

    async def fetch_meta(self, session):
        params = {"id": self.identifier}
        endpoint = self.api_url + self.__class__.__name__.lower()+"_show"
        async with session.get(endpoint, params=params) as resp:
            if not resp.headers.get('content-type'):
                return {}
            if resp.headers.get('content-type').split(';')[0] == 'text/html':
                print(resp)
                print(await resp.text())
                return {}
            data = await resp.json()
            return data

    async def generate(self, session):
        retries = 0
        while retries < self.max_retries:
            payload = await self.fetch_meta(session)
            if len(payload.keys()) == 0:
                retries += 1
                continue
            if payload['success']:
                self.metadata = payload['result']
                return
            elif payload['error']:
                print(payload['error'])
                if payload['error']['__type'] == 'Authorization Error':
                    return
                retries += 1
        
class Package(CKANEntitySearchable):
    pass

class Resource(CKANEntityNonSearchable):
    pass
class Tag(CKANEntityNonSearchable):
    pass

class Group(CKANEntityNonSearchable):
    pass

class Organization(CKANEntityNonSearchable):
    pass
    
@attr.s
class Vocabulary(object):
    api_url = attr.ib(validator=lambda *args: url(args[-1]))
    max_retries = attr.ib(validator=instance_of(int))
    metadata = attr.ib(default=attr.Factory(dict))
    name = attr.ib(init=False)
    @name.default
    def _extract_name(self):
        return self.metadata['name']

@attr.s
class CKAN(object):
    # static row limit
    limit = 10
    # static max_retries
    max_retries = 4
    root_url = attr.ib()
    persistence_folder = attr.ib()
    api = attr.ib()
    package_list = attr.ib(default=attr.Factory(list))
    resource_list = attr.ib(default=attr.Factory(list))
    group_list = attr.ib(default=attr.Factory(list))
    tag_list = attr.ib(default=attr.Factory(list))
    organization_list = attr.ib(default=attr.Factory(list))
    vocabulary_list = attr.ib(default=attr.Factory(list))
    edges = attr.ib(init=False, default=attr.Factory(list))

    @api.default
    def default_api(self):
        return self.root_url + "/api/3/action/"
    
    async def fetch(self, session, remote_url, params={}, method='GET'):
        retries = 0
        while retries < self.max_retries:
            try:
                async with session.request(method, remote_url, params=params) as resp:
                    if resp.headers.get('content-type').split(';')[0] == 'text/html':
                        print(await resp.html())
                        return {}
                    elif resp.headers.get('content-type').split(';')[0] != 'application/json':
                        print(await resp.text())
                        return {}
                    data = await resp.json()
                    return data['result']
            except Exception as e:
                retries += 1
                asyncio.sleep(pow(2, retries))
        return {}

    async def wrap_in_sem(self, sem, future):
        async with sem:
            return await future

    async def fetch_entity_list(self, session, klass, params):
        endpoint = self.api+klass.__name__.lower()+'_search'
        entity_resp = await self.fetch(session, endpoint, params)
        if 'results' in entity_resp.keys():
            return list(map(partial(klass, self.api), entity_resp['results']))
        
    async def future_empty_list(self):
        return []

    async def generate_vertex_class(self, sem, session, klass):
        endpoint = self.api+klass.__name__.lower()+'_search'
        entity_resp = await self.fetch(session, endpoint)
        if not 'count' in entity_resp.keys():
            return [self.future_empty_list()]
        count = entity_resp['count']
        entities = entity_resp['results']
        tasks = []
        for i in range(len(entities), count, self.limit):
            # if i > 10:
                # break
            params = {"rows": 10, "start": i}
            future = self.fetch_entity_list(session, klass, params)
            task = asyncio.ensure_future(self.wrap_in_sem(sem, future))
            tasks.append(task)
        return tasks



    async def generate_vertices(self):
        RETRIES=4
        async with ClientSession() as session:
            sem = asyncio.Semaphore(10)

            search_cats = {Package: []}
            list_cats = {Group: [], Tag: [], Organization: [], Vocabulary: []}
            
            for klass in search_cats.keys():
                search_cats[klass] = await self.generate_vertex_class(sem, session, klass)
            for klass, tasks in search_cats.items():
                klass_entities = [await f
                    for f in tqdm.tqdm(asyncio.as_completed(tasks),
                        desc=klass.__name__, total=len(tasks))]
                # gather the lists, then reduce them into one big list
                prop_name = klass.__name__.lower()+'_list'
                setattr(self, prop_name, reduce(add, klass_entities, []))
            # # static arguments passed to all constructors
            s_args = [self.api, RETRIES]
            resources = reduce(add, map(lambda p: p.metadata['resources'], self.package_list), [])
            self.resource_list = list(map(lambda r: Resource(*s_args, r['id']), resources))
            res_tasks = []
            for i, res in enumerate(self.resource_list):
                # if i > 10000:
                #     break
                task = asyncio.ensure_future(self.wrap_in_sem(sem, res.generate(session)))
                res_tasks.append(task)
            [await f
                for f in tqdm.tqdm(asyncio.as_completed(res_tasks),
                    desc="Resource", total=len(res_tasks))]
            for klass in list_cats.keys():
                entity_group = klass.__name__.lower() + '_list'
                endpoint = self.api+entity_group
                entity_names = await self.fetch(session, endpoint)
                entities = list(map(partial(klass, *s_args), entity_names))
                for i, ent in enumerate(entities):
                    # if i > 10:
                    #     break
                    if ent.__class__ == Vocabulary:
                        break
                    task = asyncio.ensure_future(self.wrap_in_sem(sem, ent.generate(session)))
                    list_cats[klass].append(task)
                setattr(self, entity_group, entities)
                
            for klass, tasks in list_cats.items():
                print(klass)
                print(len(tasks))
                [await f
                    for f in tqdm.tqdm(asyncio.as_completed(tasks),
                        desc=klass.__name__, total=len(tasks))]
            

    def generate_edges(self):
        for package in self.package_list:
            # <package> published by <organization>
            # <resource> resource_of <package>
            # <package> tagged as <tag>
            pack_meta = package.metadata
            if 'tag' in pack_meta.keys():
                for tag in pack_meta.get('tags', []):
                    self.edges.append({
                        "type": "tagged_as", "in": tag['id'],
                        "out": pack_meta['id']
                    })
            if 'organization' in pack_meta.keys():
                self.edges.append({
                    "label": "published_by", "out": pack_meta['id'],
                    "in": pack_meta['organization']
                })
            for resource in pack_meta.get('resources', []):
                self.edges.append({
                    "label": "resource_for", "out": resource['id'],
                    "in": pack_meta['id']
                })
            for group in pack_meta.get('groups', []):
                self.edges.append({
                    "label": "part_of", "out": pack_meta['id'],
                    "in": group['id']
                })
            for res in pack_meta.get('resources', []):
                self.edges.append({
                    "label": "resource_of", "out": res['id'],
                    "in": pack_meta['id']
                })

    def no_metadata(obj, attr, val):
        if attr.name in ["metadata", 'edges']:
            return False
        return True
    def serialize(self):
        attribs = map(lambda a: a.name, self.__attrs_attrs__)
        e_attribs = filter(lambda a: a.endswith("_list"), attribs)
        entities = map(lambda a: (a, self.__getattribute__(a)), e_attribs)
        # save full metadata lists
        for k, v in entities:
            with jsonlines.open("{}/{}.jsonl".format(self.persistence_folder, k), "w",
                dumps=partial(json.dumps, default=to_serializable)) as writer:
                writer.write(v)
        # save manifest
        with jsonlines.open("{}/edges.jsonl".format(self.persistence_folder), "w") as writer:
            writer.write(self.edges)
        serialized_ckan = attr.asdict(self, filter=self.no_metadata)
        with open("{}/ckan.json".format(self.persistence_folder), "w") as f:
            json.dump(serialized_ckan, f)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        exit()
    else:
        root_url = sys.argv[1]
        persistence_folder = sys.argv[2]
        root = CKAN(root_url, persistence_folder)
        loop = asyncio.get_event_loop()
        now = time.time()
        print(root)
        future = asyncio.ensure_future(root.generate_vertices())
        loop.run_until_complete(future)
        print(time.time() - now)
        root.generate_edges()
        root.serialize()
