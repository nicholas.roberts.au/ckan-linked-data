# Purpose
Selecting datasets to combine takes a long while - you might decide you want the locations and establishment dates of hospitals, population statistics by LGA, and the location of police stations.

You'd find each of them, figure out if they can be linked (columns match, they cover the same geographical areas, they cover the same time periods), and you'll assume they're complete or have the right metadata.

# What are we trying to answer?
1. What are the headers on this set of csvs, do they have any obvious matches?
2. How much do the members of this set of datasets overlap in time, or space?
3. Where there are column matches, what fraction of the data for each column match?
4. For any set of organizations, find the giant component of linkable datasets - you want to enrich your insights by cross-organization synthesis.
5. For a given set of tags, get the datasets that are machine readable, have at least one linkable attribute, and display them as a network of tags connected to datasets, connected to organizations and to other datasets. You still want to be able to show non-obviously linkable datasets, these should be toggleable or grayed out.